<?php

require('job_card.php');

define( "DATA_DIR", "/mnt/test");
define( "PROCESS_LIST", "SFA-ProcessList");
define( "REDIS_SERVER", "127.0.0.1");

//Connecting to Redis server on localhost
$redis = new Redis();
if( $redis->connect('127.0.0.1', 6379) !== true ) {

        // some other code to handle connection problem
        die( "Cannot connect to redis server.\n" );
}


// Get listing of data dir.
$pattern = DATA_DIR . "/*.txt";
$file_list = glob ( $pattern );

foreach ($file_list as $file) {

    // Get next job number from Redis
    $jobNum = $redis->incr('JobNum');

    $jobID = REDIS_SERVER . $jobNum;

    // Set up job card
    $jobCard = new JobCard();
    $jobCard->setJobID( REDIS_SERVER, $jobNum );
    $jobCard->setFileName( $file );

    // Push onto Redis
    // Note: Redis does not understand php data structures
    //       so first convert to a json string. 
    // 
    $json_string = json_encode(serialize($jobCard));

    printf( "  [%s] SUB JOB: <%s> - (%s)\n", PROCESS_LIST,
            $jobCard->getJobID(), basename($jobCard->getFileName()) );
    $redis->lpush(PROCESS_LIST, $json_string );
}

?>

