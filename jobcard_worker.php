<?php

require('job_card.php');

$jobCard = new JobCard();

$worker= new GearmanWorker();
$worker->addServer();

$worker->addFunction("jobcard", "process_jobcard", $jobCard);

//
// Process a job card.
//
// A job card represents a job to complete.
// A job will be made up of one or more tasks that must be completed
// in sequential order.
// 
// Any number of jobs may be processed simultaeously.
//
do {
    printf( "    [JobCard] WAIT\n" );

    $worker->work();
    $return = $worker->returnCode();

    if ($return != GEARMAN_SUCCESS) {
        printf( "ERROR: %d\n", $worker->returnCode() );
    }
} while($return == GEARMAN_SUCCESS );


function process_jobcard($job, &$jobCard)
{
    // 
    // For this demo we are building into this routine knowledge of what
    // tasks are required for this particular jhob.
    //
    // Future variants will have the task list embedded in the job card.
    //
    printf( "    [JobCard] RECV: %s\n", $job->handle() );
    $workload = $job->workload();
    $workload_size = $job->workloadSize();

    // Reform the JobCard object.
    $jobCard = unserialize(json_decode($workload));
//print_r( $job );

    printf( "    [JobCard] PROCESS JOB : %s\n", $jobCard->getFileName() );

    // 
    // Generate new gearman client to submit the job tasks
    //
    $client= new GearmanClient();
    $client->addServer();

    // 
    // Execute the job tasks synchronously
    // 
    printf( "    [JobCard]   SUBMIT TASK : Extraction\n" );
    $result = $client->do("extract", $workload);

    // Check for various return packets and errors.
    switch($client->returnCode())
    {
        case GEARMAN_WORK_DATA:
          printf( "    [JobCard]   TASK DATA : '%s'\n", $result );
          break;
        case GEARMAN_WORK_STATUS:
          list($numerator, $denominator)= $gmclient->doStatus();
          printf( "    [JobCard]   TASK STATUS : %d/%d\n", $numerator, $denominator );
          break;
        case GEARMAN_WORK_FAIL:
          printf( "    [JobCard]   TASK FAILED\n" );
          break;
        case GEARMAN_SUCCESS:
          printf( "    [JobCard]   TASK SUCCESS\n" );
          break;
        default:
          printf( "    [JobCard]   TASK RETURN - '%s'\n", $gmclient->returnCode() );
          break;
    }

    printf( "    [JobCard]   SUBMIT TASK : Reverse\n" );
    $result = $client->do("reverse", $workload);
    // Check for various return packets and errors.
    switch($client->returnCode())
    {
        case GEARMAN_WORK_DATA:
          printf( "    [JobCard]   TASK DATA : '%s'\n", $result );
          break;
        case GEARMAN_WORK_STATUS:
          list($numerator, $denominator)= $gmclient->doStatus();
          printf( "    [JobCard]   TASK STATUS : %d/%d\n", $numerator, $denominator );
          break;
        case GEARMAN_WORK_FAIL:
          printf( "    [JobCard]   TASK FAILED\n" );
          break;
        case GEARMAN_SUCCESS:
          printf( "    [JobCard]   TASK SUCCESS\n" );
          break;
        default:
          printf( "    [JobCard]   TASK RETURN - '%s'\n", $gmclient->returnCode() );
          break;
    }

    printf( "    [JobCard]   SUBMIT TASK : Capitalise\n" );
    $result = $client->do("capitalise", $workload);
    // Check for various return packets and errors.
    switch($client->returnCode())
    {
        case GEARMAN_WORK_DATA:
          printf( "    [JobCard]   TASK DATA : '%s'\n", $result );
          break;
        case GEARMAN_WORK_STATUS:
          list($numerator, $denominator)= $gmclient->doStatus();
          printf( "    [JobCard]   TASK STATUS : %d/%d\n", $numerator, $denominator );
          break;
        case GEARMAN_WORK_FAIL:
          printf( "    [JobCard]   TASK FAILED\n" );
          break;
        case GEARMAN_SUCCESS:
          printf( "    [JobCard]   TASK SUCCESS\n" );
          break;
        default:
          printf( "    [JobCard]   TASK RETURN - '%s'\n", $gmclient->returnCode() );
          break;
    }

    printf( "    [JobCard] JOB END\n" );
}

?>
