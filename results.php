<?php

require('job_card.php');

define( "RESULTS_LIST", "SFA-JobResults");
//define( "RESULTS_LIST", "SFA-ProcessList");

define( "ERROR_WAIT", 5);
define( "MAX_RETRY", 5);


//Connecting to Redis server on localhost
$redis = new Redis();
if( $redis->connect('127.0.0.1', 6379) !== true ) {

        // some other code to handle connection problem
        die( "Cannot connect to redis server.\n" );
}

// Determine which queue to listen on.
$queueName = RESULTS_LIST;
$errorCount = 0;


// Read off results as they arise.
while ( true ) {

    try {
        printf( "  [%s] WAIT\n", $queueName );
        $request = $redis->blPop($queueName, 0);

        printf( "\n  [%s] RECV\n", $queueName );

	// Call succeded - reset error count.
        $errorCount = 0;

	// Convert payload back into a JobCard object.
        $jobCard = $request[1];
        $jobCard = unserialize(json_decode($jobCard));

        announce_completion( $queueName, $jobCard );
    }
    catch( RedisException $e ) {

        printf( "\nERROR: Caught exception\n" );
        printf( "Exception text - '%s':\n", $e->getMessage() );

        if ( $errorCount > MAX_RETRY ) {
            printf( "Exceeded retry count... Aborting!\n" );
            exit( 1 );
        } else {
            $errorCount++;
            printf( "Retrying n %d seconds...\n\n", ERROR_WAIT );
            sleep( ERROR_WAIT );
        }
    }

} 

function announce_completion( $queueName, $jobCard ) {

    printf( "  [%s] JOB END: <%s> - %s (%s)\n", 
            $queueName, 
            $jobCard->getJobID(), 
            $jobCard->getStatus(), 
            $jobCard->getResult() );
}

?>

