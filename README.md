# README #

Gearman/Redis Demo.

### Example use of Redis for message queue and Gearman for process control ###

Apps are:

### Client - main_client.php ###

This app reads in a list of files held in the directory defined under DATA_DIR
Puts job on Redis queue

### Queue Processor - job_submission.php ###

  Reads jobs off the Redis queue and submits each job to Gearman

### Task_Manager - jobcard_worker.php ###

  Receives a job from Gearman.
  Requests individual tasks that make up a job

### Worker - task_worker.php ###

  Perfoms individual taks.


### How do I get set up? ###

* Install Redis
* Install Gearman
* Run apps main_client.php, job_submission.php, jobcard_worker.php and task_worker.php

### Who do I talk to? ###

* Paul
